package co.uk.bensimms.tests.pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPageObjects {
   @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[2]/ul/li[1]/a/span")
   private WebElement joinButton;
   @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[2]/ul/li[2]/a/span")
   private WebElement homeButton;
   @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[2]/ul/li[3]/a")
   private WebElement forumsButton;

   public boolean isJoinButtonPresent() {
      joinButton.isDisplayed();
      return true;
   }

   public boolean isHomeButtonPresent() {
      homeButton.isDisplayed();
      return true;
   }

   public boolean isForumsButtonPresent() {
      forumsButton.isDisplayed();
      return true;
   }
}