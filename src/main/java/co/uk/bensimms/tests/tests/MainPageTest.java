package co.uk.bensimms.tests.tests;

import co.uk.bensimms.tests.pageObjects.MainPageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;


public class MainPageTest {
   public static void main(String[] args) throws InterruptedException {
      WebDriver senseDriver = new FirefoxDriver();
      MainPageObjects mainPage = PageFactory.initElements(senseDriver, MainPageObjects.class);
      senseDriver.get("http://www.theditcrowd.com");

      mainPage.isJoinButtonPresent();
      mainPage.isHomeButtonPresent();
      mainPage.isForumsButtonPresent();

      senseDriver.quit();
   }
}